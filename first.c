#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct Letter {
	char value;
	struct Letter *next;
	size_t childs;
};
#define LetterSize sizeof(struct Letter)

void addChild(struct Letter *tree, char val) {
        tree->next = (struct Letter*) realloc( tree->next, LetterSize*(++tree->childs));
	tree->next[tree->childs-1].value = val;
	tree->next[tree->childs-1].next = NULL;
	tree->next[tree->childs-1].childs = 0;
}

void addStringToTree(struct Letter *tree, char *str, size_t size) {
	size_t i=0;
	if ( size == 0 ) return;
	for ( i=0; i<tree->childs; ++i ) {
		if ( tree->next[i].value == *str ) {
			addStringToTree((tree->next+i), str+1, size-1);
			return;
		}
	}
	addChild(tree, *str);
	addStringToTree(tree->next+tree->childs-1, str+1, size-1);
}

struct LetterList {
	struct Letter *heads;
	size_t size;
};

void initList(struct LetterList *ll) {
	ll->heads=NULL;
	ll->size = 0;
}

void addHead(struct LetterList *lt, char val) {
	lt->heads = (struct Letter*) realloc(lt->heads, LetterSize*(++lt->size));
	lt->heads[lt->size-1].value = val;
	lt->heads[lt->size-1].next = NULL;
	lt->heads[lt->size-1].childs = 0;
}

void addStringToList(struct LetterList *lt, char *str, size_t size) {
	size_t j=0;
	if ( size == 0 ) return;
	for ( j=0; j < lt->size; ++j ) {
		if ( lt->heads[j].value ==  *str ) {
			addStringToTree(lt->heads+j, str+1, size-1);
			return;
		}
	}
	addHead(lt, *str);
	addStringToTree(lt->heads+lt->size-1, str+1, size-1);
}

int checkTree(struct Letter *l, char *str, size_t size) {
	int i;
	if(size==0) return 1;
	for ( i=0; i < l->childs; ++i ) {
		if ( l->next[i].value == *str )
			return checkTree(l->next+i, str+1, size-1);
	}
	return -1;
}

int checkList(struct LetterList *ll, char *str, size_t size) {
	int i;
	if ( size == 0 ) return 1;
        for ( i=0; i < ll->size; ++i ) {
		if ( ll->heads[i].value == *str )
	                return checkTree(ll->heads+i, str+1, size-1);
        }
        return -1;
}

void printTree(struct Letter* l, int level) {
	int i;
	if(l->value == '\0') printf("-%d:end-", level);
	else printf("-%d:%c-", level, l->value);
	for ( i=0; i < l->childs; ++i ) {
		printTree(l->next+i, level+1);
	}
}

void printList(struct LetterList* ll) {
	int i;
	for ( i=0; i < ll->size; ++i ) {
		printTree(ll->heads+i, 0);
	}
	printf("\n");
}

int main(int argc, char *argv[]) {

	if ( argc < 2 ) return -1;
	char filename[20];
	char str[256];
	char *line = NULL;
	int linesize=0, len=0;
	struct LetterList tree;
	initList(&tree);
	strcpy(filename, argv[1]);
	
	FILE* file = fopen(filename, "r");
	if ( file == NULL ) return -1;
	while ((linesize = getline(&line, &len, file)) != -1) {
		line[linesize-1]='\0';
		addStringToList(&tree, line, linesize);
	}
	free(line);
	printList(&tree);
	fclose(file);

	while(1) {
		printf("~> ");
		if(scanf("%s", str)!=1)
			return -1;
		if ( strcmp(str, "exit") == 0 ) {
			return 0;
		}
		fflush(stdin);
		len = strlen(str);
		if ( checkList(&tree, str, len+1) > 0 ) printf("YES\n");
		else printf("NO\n");
	}
	return 0;
}
