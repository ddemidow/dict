#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define FIRST 32
#define LAST 127
#define SIZE (LAST-FIRST+1)
#define SHIFT(c) (c!='\0'?c-FIRST:SIZE-1)

struct Letter {
	char value;
	struct Letter *next[SIZE];
};
#define LetterSize sizeof(struct Letter)

void initLetter(struct Letter *l) {
	size_t i;
	l->value = '\0';
	for ( i=0; i<SIZE; ++i ) l->next[i] = NULL;
}

struct Letter* addChild(struct Letter *tree, char val) {
	int shift = SHIFT(val);
	struct Letter *pchild = tree->next[shift];
	if ( !pchild ) { 
		pchild = malloc(LetterSize);
		initLetter(pchild);
	        pchild->value = val;
		tree->next[shift] = pchild;
	}
	return pchild;
}

void addStringToTree(struct Letter *tree, char *str, size_t size) {
	if ( size == 0 ) return;
	struct Letter *pc = addChild(tree, *str);
	addStringToTree(pc, str+1, size-1);
}

int checkTree(struct Letter *l, char *str, size_t size) {
	if ( size==0 ) return 1;
	int shift = SHIFT(*str);
	struct Letter *pc = l->next[shift];
	if ( pc ) return checkTree(pc, str+1, size-1);
	return -1;
}

void printTreeLevel(struct Letter* l, int level) {
	int i;
	if(l->value == '\0') printf("-%d:end-", level);
	else printf("-%d:%c-", level, l->value);
	for(i=0; i<SIZE; ++i) {
		if ( l->next[i] )  printTreeLevel(l->next[i], level+1);
	}
}

void printTree(struct Letter *l) {
	printTreeLevel(l, 0);
	printf("\n");
}

void freeTree(struct Letter *l) {
	size_t i;
	struct Letter *pc;
        for(i=0; i<SIZE; ++i) {
		pc = l->next[i];
                if ( pc ) freeTree(pc);
        }
	free(l);
}

int main(int argc, char *argv[]) {

	if ( argc < 2 ) return -1;
	char filename[20];
	char str[256];
	char *line = NULL;
	int linesize=0, len=0;
	struct Letter *tree = malloc(LetterSize);
	initLetter(tree);
	strcpy(filename, argv[1]);
	
	FILE* file = fopen(filename, "r");
	if ( file == NULL ) return -1;
	while ((linesize = getline(&line, &len, file)) != -1) {
		line[linesize-1]='\0';
		addStringToTree(tree, line, linesize);
	}
	free(line);
	/*printTree(&tree);*/
	fclose(file);

	while(1) {
		printf("~> ");
		if(scanf("%s", str)!=1) break;
		if ( strcmp(str, "exit") == 0 ) break;
		fflush(stdin);
		len = strlen(str);
		if ( checkTree(tree, str, len+1) > 0 ) printf("YES\n");
		else printf("NO\n");
	}
	freeTree(tree);
	return 0;
}
